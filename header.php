<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->
        <!-- Font-Awesome CSS -->
        <link rel="stylesheet" type="text/css" href="font-awesome-4.3.0/css/font-awesome.min.css">
        <!-- Custom Fonts -->
        <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/65896ee2-c57e-41ac-a62e-9f55f0e954f0.css"/>
        <!-- Typebase CSS -->
        <link rel="stylesheet" href="css/typebase.css" />
        <!-- Slick CSS -->
        <link rel="stylesheet" type="text/css" href="slick-1.5.0/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="slick-1.5.0/slick/slick-theme.css">
        <!-- FancyBox CSS -->
        <link rel="stylesheet" href="fancyapps/source/jquery.fancybox.css" type="text/css" media="screen" />
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/mobile.css">
        
        <!-- Modernizer JS -->
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    </head>
    <body>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <!--   <nav id="top-nav" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="img/logo/logo-one.png" alt=""></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="classes.php">Classes</a>
                        </li>
                        <li>
                            <a href="workshop.php">Workshops &amp; Events </a>
                        </li>
                        <li>
                            <a href="studio.php">Studio</a>
                        </li>
                        <li>
                            <a href="instructors.php">Instructors</a>
                        </li>
                        <li>
                            <a href="community.php">Community</a>
                        </li>
                        <li>
                            <a href="faq.php">FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav> -->

        