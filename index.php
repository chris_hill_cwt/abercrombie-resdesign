<?php include 'header.php'; ?><!-- header -->

<!-- Jumbotron
============================== -->
<div id="home-jumbotron" class="jumbotron">
	<div class="container vert-align">
		<div class="content center">
			<p class="cap">Exclusive First Look</p>
			<p>Get new arrivals before anyone else!</p>
			<h1 class="cap">20% Off</h1>
			<h3 class="cap">Entire Purchase</h3>
			<a href="#stretch" class="btn btn-clear cap">Shop Men</a>
		</div>
	</div>
</div><!-- /#home-jumbotron.jumbotron -->

<!-- Mens Strech -->
<section id="stretch">
	<div class="container">
		<!-- Section Title -->
		<div class="title center">
			<h1 class="cap">The New A&amp;F Stretch </h1>
			<hr>
			<p>Lorem ipsum Exercitation fugiat proident amet cupidatat anim officia elit laborum ullamco non ut elit mollit amet sunt laboris dolor aliqua dolore ut consequat fugiat ex aute irure laborum consequat et sint do laborum velit dolore consectetur commodo Excepteur laboris sunt.</p>
		</div><!-- /.title -->

		<div class="filler">
			<div class="image-contain">
				<img src="img/filler/fill-1.jpeg" alt="">
			</div>
		</div>

		<!-- Section Products  -->
		<div class="row">
			<div class="title center">
				<h3 class="cap">Choose your pair</h3> 
				<p class="cap">20% off entire purchase</p>
			</div><!-- /.title -->

			<div class="arrows hidden-xs">
				<div class="buttons center">
					<a href="#" title="" class="btn btn-blk"><i class="fa fa-angle-left"></i></a>
					<a href="" title="" class="btn btn-blk-clear"><i class="fa fa-angle-right"></i></a>
				</div><!-- /.buttons -->
			</div>
			<div class="col-sm-3">
				<div class="home-product-container">
					<img src="img/products/p-1.jpeg" alt="">
					<div class="product-content">
						<h4>A&amp;F Skinny Jeans</h4>
						<p>Cillum laborum</p>
						<p>Aliquip</p>
						<h4>$78</h4>
					</div><!-- /.product-content -->
				</div>
			</div><!-- /.col-sm-3 -->
			<div class="col-sm-3">
				<div class="home-product-container">
					<img src="img/products/p-2.jpeg" alt="">
					<div class="product-content">
						<h4>A&amp;F Skinny Jeans</h4>
						<p>Cillum laborum</p>
						<p>Aliquip</p>
						<h4>$64</h4>
					</div><!-- /.product-content -->
				</div>
			</div><!-- /.col-sm-3 -->
			<div class="col-sm-3">
				<div class="home-product-container">
					<img src="img/products/p-3.jpeg" alt="">
					<div class="product-content">
						<h4>A&amp;F Skinny Jeans</h4>
						<p>Cillum laborum</p>
						<p>Aliquip</p>
						<h4>$66</h4>
					</div><!-- /.product-content -->
				</div>
			</div><!-- /.col-sm-3 -->
			<div class="col-sm-3">
				<div class="home-product-container">
					<img src="img/products/p-4.jpeg" alt="">
					<div class="product-content">
						<h4>A&amp;F Skinny Jeans</h4>
						<p>Cillum laborum</p>
						<p>Aliquip</p>
						<h4>$74</h4>
					</div><!-- /.product-content -->
				</div>
			</div><!-- /.col-sm-3 -->
		</div>
	</div>
</section>

<?php include 'footer.php'; ?><!-- footer -->